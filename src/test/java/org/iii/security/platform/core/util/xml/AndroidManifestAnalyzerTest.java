package org.iii.security.platform.core.util.xml;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class AndroidManifestAnalyzerTest {
	
	private static AndroidManifestAnalyzer androidManifestAnalyzer;
	
	@BeforeClass
	public static void beforeClass() {
		androidManifestAnalyzer = new AndroidManifestAnalyzer();
		androidManifestAnalyzer.loadManifest("src/test/resources/TestAndroidManifest.xml");
	}

	@Test
	@Ignore
	public void testForObtainPermissions() {
		List<String> permissions = androidManifestAnalyzer.extractAllPermissions();
		assertEquals(28, permissions.size());
		assertEquals("com.evernote", androidManifestAnalyzer.getPackageName());
	}

}
