package org.iii.security.platform.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Entity
@Table(name = "emulation_environment_info")
public class EmulationEnv implements Serializable {

	private static final long serialVersionUID = 5758570098301171989L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "emulator_name")
	private String emulatorName;
	
	@Column(name = "scheduled")
	private boolean scheduled;
	
	@Column(name = "emulator_launched")
	private boolean emulatorLaunched;
	
	@Column(name = "logcat_launched")
	private boolean logcatLaunched;
	
	@Column(name = "can_install_apk")
	private boolean canInstallApk;
	
	@Column(name = "apk_installed")
	private boolean apkInstalled;
	
	@Column(name = "analytics_project_id")
	private long analyticsProjectId;
	
	@Column(name = "apk_for_analytics", nullable = true)
	private String apkForAnalytics;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getEmulatorName() {
		return emulatorName;
	}
	public void setEmulatorName(String emulatorName) {
		this.emulatorName = emulatorName;
	}

	public boolean isScheduled() {
		return scheduled;
	}
	public void setScheduled(boolean scheduled) {
		this.scheduled = scheduled;
	}

	public boolean isEmulatorLaunched() {
		return emulatorLaunched;
	}
	public void setEmulatorLaunched(boolean emulatorLaunched) {
		this.emulatorLaunched = emulatorLaunched;
	}

	public boolean isLogcatLaunched() {
		return logcatLaunched;
	}
	public void setLogcatLaunched(boolean logcatLaunched) {
		this.logcatLaunched = logcatLaunched;
	}

	public boolean isCanInstallApk() {
		return canInstallApk;
	}
	public void setCanInstallApk(boolean canInstallApk) {
		this.canInstallApk = canInstallApk;
	}
	
	public boolean isApkInstalled() {
		return apkInstalled;
	}
	public void setApkInstalled(boolean apkInstalled) {
		this.apkInstalled = apkInstalled;
	}

	public long getAnalyticsProjectId() {
		return analyticsProjectId;
	}
	public void setAnalyticsProjectId(long analyticsProjectId) {
		this.analyticsProjectId = analyticsProjectId;
	}

	public String getApkForAnalytics() {
		return apkForAnalytics;
	}
	public void setApkForAnalytics(String apkForAnalytics) {
		this.apkForAnalytics = apkForAnalytics;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (analyticsProjectId ^ (analyticsProjectId >>> 32));
		result = prime * result + ((apkForAnalytics == null) ? 0 : apkForAnalytics.hashCode());
		result = prime * result + (apkInstalled ? 1231 : 1237);
		result = prime * result + (canInstallApk ? 1231 : 1237);
		result = prime * result + (emulatorLaunched ? 1231 : 1237);
		result = prime * result + ((emulatorName == null) ? 0 : emulatorName.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (logcatLaunched ? 1231 : 1237);
		result = prime * result + (scheduled ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmulationEnv other = (EmulationEnv) obj;
		if (analyticsProjectId != other.analyticsProjectId)
			return false;
		if (apkForAnalytics == null) {
			if (other.apkForAnalytics != null)
				return false;
		} else if (!apkForAnalytics.equals(other.apkForAnalytics))
			return false;
		if (apkInstalled != other.apkInstalled)
			return false;
		if (canInstallApk != other.canInstallApk)
			return false;
		if (emulatorLaunched != other.emulatorLaunched)
			return false;
		if (emulatorName == null) {
			if (other.emulatorName != null)
				return false;
		} else if (!emulatorName.equals(other.emulatorName))
			return false;
		if (id != other.id)
			return false;
		if (logcatLaunched != other.logcatLaunched)
			return false;
		if (scheduled != other.scheduled)
			return false;
		return true;
	}
	
	public EmulationEnv initialize() {
		this.scheduled = false;
		this.emulatorLaunched = false;
		this.logcatLaunched = false;
		this.canInstallApk = false;
		this.apkInstalled = false;
		this.analyticsProjectId = 0;
		this.apkForAnalytics = "";
		return this;
	}
	
	public boolean canRunAnalysis() {
		return (this.scheduled && 
				this.emulatorLaunched && 
				this.logcatLaunched && 
				this.canInstallApk && 
				this.apkInstalled);
	}

	@Override
	public String toString() {
		return "EmulationEnv [id=" + id + ", emulatorName=" + emulatorName
				+ ", scheduled=" + scheduled + ", emulatorLaunched="
				+ emulatorLaunched + ", logcatLaunched=" + logcatLaunched
				+ ", apkInstalled=" + apkInstalled + ", canInstallApk="
				+ canInstallApk + ", analyticsProjectId=" + analyticsProjectId
				+ ", apkForAnalytics=" + apkForAnalytics + "]";
	}

}
