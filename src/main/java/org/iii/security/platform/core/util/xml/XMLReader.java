package org.iii.security.platform.core.util.xml;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class XMLReader {
	
	private static final Logger logger = LoggerFactory.getLogger(XMLReader.class);
	
	private XmlPullParser xmlParser = null;

	public XMLReader() {
		logger.debug("Creating XML parser...");
		try {
			XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
			xmlPullParserFactory.setNamespaceAware(true);
			xmlParser = xmlPullParserFactory.newPullParser();
			logger.debug("XML parser has created successfully.");
		} catch (Exception exception) {
			logger.debug("Create XMLPullParser failed, the error message is: {}", exception.getMessage());
		}
	}
	
	public boolean setXML(String xmlFile) throws IOException {
		boolean set = false;
		logger.debug("Reading xml from file: {}...", xmlFile);
		try {
			FileInputStream fileInputStream = new FileInputStream(new File(xmlFile));
			InputStream inputStream = new BufferedInputStream(fileInputStream);
			xmlParser.setInput(inputStream, "UTF-8");
			set = true;
			logger.debug("Read xml resource successfully.");
		} catch (Exception exception) {
			logger.debug("Set xml resource failed, the error message is: {}", exception.getMessage());
		}
		return set;
	}
	
	public int getNextEvent() throws IOException {
		int nextEvent = -1;
		try {
			nextEvent = xmlParser.next();
		} catch (Exception exception) {
			logger.debug("Get next event failed, the error message is: {}", exception.getMessage());
		}
		return nextEvent;
	}
	
	public String getNextText() throws IOException {
		String nextText = null;
		try {
			nextText = xmlParser.nextText();
		} catch (Exception exception) {
			logger.debug("Get next text failed, the error message is: {}", exception.getMessage());
		}
		return nextText;
	}
	
	public int getEventType() throws IOException {
		int eventType = -1;
		try {
			eventType = xmlParser.getEventType();
		} catch (Exception exception) {
			logger.debug("Get event type failed, the error message is: {}", exception.getMessage());
		}
		return eventType;
	}
	
	public String getName() {
		return xmlParser.getName();
	}
	
	public String getText() {
		return xmlParser.getText();
	}
	
	public int getAttributeCount() {
		return xmlParser.getAttributeCount();
	}
	
	public String getAttributeName(int attrIndex) {
		return xmlParser.getAttributeName(attrIndex);
	}
	
	public String getAttributeValueWithAttrIndex(int attrIndex) {
		return xmlParser.getAttributeValue(attrIndex);
	}
	
	public String getAttributeValueWithAttrName(String name) {
		String attrValue = xmlParser.getAttributeValue(null, name);
		return attrValue;
	}
	
	public int getDepth() {
		return xmlParser.getDepth();
	}
	
	public void getNext() {
		try {
			xmlParser.next();
		} catch (XmlPullParserException e) {
			logger.debug("Something wrong when get next element, the error message is: {}", e.getMessage());
		} catch (IOException e) {
			logger.debug("Something wrong when get next element, the error message is: {}", e.getMessage());
		}
	}
	
}
