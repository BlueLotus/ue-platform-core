package org.iii.security.platform.core.persistence;

import java.util.List;

import org.iii.security.platform.core.domain.TreeDiagramResult;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Repository
public interface TreeDiagramResultRepository extends CrudRepository<TreeDiagramResult, Long> {
	
	List<TreeDiagramResult> findByProjectId(long projectId);

}
