package org.iii.security.platform.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Entity
@Table(name = "project_analysis_tree_result")
public class TreeDiagramResult implements Serializable {

	private static final long serialVersionUID = 7212329918613541009L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "project_id")
	private long projectId;
	
	@Lob
	@Column(name = "result")
	private String result;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (int) (projectId ^ (projectId >>> 32));
		result = prime * result + ((this.result == null) ? 0 : this.result.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TreeDiagramResult other = (TreeDiagramResult) obj;
		if (id != other.id)
			return false;
		if (projectId != other.projectId)
			return false;
		if (result == null) {
			if (other.result != null)
				return false;
		} else if (!result.equals(other.result))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "TreeDiagramResult [id=" + id + ", projectId=" + projectId + ", result=" + result + "]";
	}
	
}
