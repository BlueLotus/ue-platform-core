package org.iii.security.platform.core.util.process;

import java.util.ArrayList;
import java.util.List;

import org.iii.security.platform.core.domain.SystemConfig;
import org.iii.security.platform.core.persistence.SystemConfigRepository;
import org.iii.security.platform.core.util.constant.UEPlatformConstant;
import org.iii.security.platform.core.util.constant.EmulatorJobConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Component
public class ProcessBuilderGenerator {
	
	@Autowired
	SystemConfigRepository systemConfigRepository;
	
	private String emulatorName;
	private String apkName;
	private String packageName;
	private String snapshotName;
	private long projectId;

	public ProcessBuilder generateProcessBuilderWithSpecifiedScenario(int scenario) {
		ProcessBuilder processBuilder = new ProcessBuilder();
		processBuilder.command(generateCommandListWithSpecifiedScenario(scenario));
		return processBuilder;
	}
	
	private List<String> generateCommandListWithSpecifiedScenario(int scenario) {
		List<String> command = null;
		switch (scenario) {
		case EmulatorJobConstant.OBTAIN_APK_MANIFEST_ANALYZER_PROCESS:
			command = obtainCommandListForDecompileAPK();
			break;
		case EmulatorJobConstant.OBTAIN_EMULATOR_LAUNCH_PROCESS:
			command = obtainCommandListForLaunchEmulator();
			break;
		case EmulatorJobConstant.OBTAIN_LOGCAT_LAUNCHER:
			command = obtainCommandListForLogcatLauncher();
			break;
		case EmulatorJobConstant.OBTAIN_LOGCAT_LAUNCH_PROCESS:
			command = obtainCommandListForLaunchLogcat();
			break;
		case EmulatorJobConstant.OBTAIN_APK_FILE_INSTALLER_PROCESS:
			command = obtainCommandListForInstallAPK();
			break;
		case EmulatorJobConstant.OBTAIN_SYSTEM_DUMP_PROCESS:
			command = obtainCommandListForSystemDump();
			break;
		case EmulatorJobConstant.OBTAIN_ACTIVITY_SNAPSHOT_CATCHER_PROCESS:
			command = obtainCommandListForCatchActivitySnapshot();
			break;
		case EmulatorJobConstant.OBTAIN_PULL_SNAPSHOT_PROCESS:
			command = obtainCommandListForPullSnapshot();
			break;
		case EmulatorJobConstant.OBTAIN_MONKEY_RUNNER_SCRIPT_PLAYER_PROCESS:
			command = obtainCommandListForPlaybackMonkeyRunnerScript();
			break;
		case EmulatorJobConstant.OBTAIN_APK_FILE_UNINSTALLER_PROCESS:
			command = obtainCommandListForUninstallAPK();
			break;
		default:
			break;
		}
		return command;
	}
	
	private List<String> obtainCommandListForDecompileAPK() {
		SystemConfig config = systemConfigRepository.findOne(1l);
		List<String> command = new ArrayList<String>();
		command.add(config.getPathToApktool() + EmulatorJobConstant.APK_TOOL_COMMAND);
		command.add(EmulatorJobConstant.APK_TOOL_OPTION_DECOMPILE);
		command.add(EmulatorJobConstant.APK_TOOL_OPTION_OVERWRITE);
		command.add(UEPlatformConstant.APK_FILE_LOCATION + apkName);
		command.add(EmulatorJobConstant.APK_TOOL_OPTION_OUTPUT);
		command.add(UEPlatformConstant.DECOMPILED_APK_FILE_LOCATION + apkName);
		return command;
	}
	
	private List<String> obtainCommandListForLaunchEmulator() {
		SystemConfig config = systemConfigRepository.findOne(1l);
		List<String> command = new ArrayList<String>();
		command.add(config.getPathToAndroidSdk() + EmulatorJobConstant.EMULATOR_COMMAND);
		command.add(EmulatorJobConstant.EMULATOR_AVD_OPTION);
		command.add(emulatorName);
		command.add(EmulatorJobConstant.EMULATOR_SYSTEM_OPTION);
		command.add(config.getPathToTaintDroidRoom() + "\\system.img");
		command.add(EmulatorJobConstant.EMULATOR_DATA_OPTION);
		command.add(config.getPathToTaintDroidRoom() + "\\userdata.img");
		command.add(EmulatorJobConstant.EMULATOR_RAMDISK_OPTION);
		command.add(config.getPathToTaintDroidRoom() + "\\ramdisk.img");
		return command;
	}
	
	private List<String> obtainCommandListForLogcatLauncher() {
		SystemConfig config = systemConfigRepository.findOne(1l);
		List<String> command = new ArrayList<String>();
		command.add(config.getPathToAndroidSdk() + EmulatorJobConstant.ADB_COMMAND);
		command.add(EmulatorJobConstant.ADB_DEVICES_OPTION);
		return command;
	}
	
	private List<String> obtainCommandListForLaunchLogcat() {
		SystemConfig config = systemConfigRepository.findOne(1l);
		List<String> command = new ArrayList<String>();
		command.add(config.getPathToAndroidSdk() + EmulatorJobConstant.ADB_COMMAND);
		command.add(EmulatorJobConstant.ADB_LOGCAT_OPTION);
		command.add(EmulatorJobConstant.ADB_LOGCAT_VERBOSE);
		command.add(EmulatorJobConstant.ADB_LOGCAT_TIME);
		command.add(EmulatorJobConstant.ADB_LOGCAT_TAINTLOG_WARNING);
		command.add(EmulatorJobConstant.ADB_LOGCAT_ACTIVITY_MGR_INFO);
		command.add(EmulatorJobConstant.ADB_LOGCAT_ALERTSERVICE_DEBUG);
		command.add(EmulatorJobConstant.ADB_LOGCAT_ALERTRECEIVER_DEBUG);
		command.add(EmulatorJobConstant.ADB_LOGCAT_ALL_SYSTEM_MSG);
		return command;
	}
	
	private List<String> obtainCommandListForInstallAPK() {
		SystemConfig config = systemConfigRepository.findOne(1l);
		List<String> command = new ArrayList<String>();
		command.add(config.getPathToAndroidSdk() + EmulatorJobConstant.ADB_COMMAND);
		command.add(EmulatorJobConstant.ADB_INSTALL_OPTION);
		command.add(UEPlatformConstant.DIR_FOR_APK_TO_BE_INSTALLED + apkName);
		return command;
	}
	
	private List<String> obtainCommandListForSystemDump() {
		SystemConfig config = systemConfigRepository.findOne(1l);
		List<String> command = new ArrayList<String>();
		command.add(config.getPathToAndroidSdk() + EmulatorJobConstant.ADB_COMMAND);
		command.add(EmulatorJobConstant.ADB_SHELL_OPTION);
		command.add(EmulatorJobConstant.ADB_DUMPSYS);
		command.add(EmulatorJobConstant.ADB_DUMP_FOR_ACTIVITY);
		return command;
	}
	
	private List<String> obtainCommandListForCatchActivitySnapshot() {
		SystemConfig config = systemConfigRepository.findOne(1l);
		List<String> command = new ArrayList<String>();
		command.add(config.getPathToAndroidSdk() + EmulatorJobConstant.ADB_COMMAND);
		command.add(EmulatorJobConstant.ADB_SHELL_OPTION);
		command.add(EmulatorJobConstant.ADB_SHELL_SCREEN_CAPTURE);
		command.add(EmulatorJobConstant.ADB_SCREEN_CAPTURE_OPTION);
		command.add(EmulatorJobConstant.ADB_SDCARD_PATH + snapshotName);
		return command;
	}
	
	private List<String> obtainCommandListForPullSnapshot() {
		SystemConfig config = systemConfigRepository.findOne(1l);
		List<String> command = new ArrayList<String>();
		command.add(config.getPathToAndroidSdk() + EmulatorJobConstant.ADB_COMMAND);
		command.add(EmulatorJobConstant.ADB_PULL_OPTION);
		command.add(EmulatorJobConstant.ADB_SDCARD_PATH + snapshotName);
		command.add(UEPlatformConstant.ACTIVITY_SNAPSHOT_LOCATION + projectId + "\\" + snapshotName);
		return command;
	}
	
	private List<String> obtainCommandListForPlaybackMonkeyRunnerScript() {
		SystemConfig config = systemConfigRepository.findOne(1l);
		List<String> command = new ArrayList<String>();
		command.add(config.getPathToAndroidSdk() + EmulatorJobConstant.MONKEY_RUNNER_COMMAND);
		command.add(config.getPathToAndroidSdk() + EmulatorJobConstant.MONKEY_RUNNER_PLAYBACK_OPTION);
		command.add(UEPlatformConstant.MONKEY_SCRIPT_FILE_LOCATION + projectId + EmulatorJobConstant.MONKEY_RUNNER_SCRIPT_FOR_PLAY_BACK);
		return command;
	}
	
	private List<String> obtainCommandListForUninstallAPK() {
		SystemConfig config = systemConfigRepository.findOne(1l);
		List<String> command = new ArrayList<String>();
		command.add(config.getPathToAndroidSdk() + EmulatorJobConstant.ADB_COMMAND);
		command.add(EmulatorJobConstant.ADB_UNINSTALL_OPTION);
		command.add(packageName);
		return command;
	}

	public void setEmulatorName(String emulatorName) {
		this.emulatorName = emulatorName;
	}

	public void setApkName(String apkName) {
		this.apkName = apkName;
	}
	
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public void setSnapshotName(String snapshotName) {
		this.snapshotName = snapshotName;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	
}
