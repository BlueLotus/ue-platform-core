package org.iii.security.platform.core.persistence;

import org.iii.security.platform.core.domain.APKManifestInfo;
import org.springframework.data.repository.CrudRepository;

/**
 * 
 * @author Carl Alder(C.A.)
 *
 */
public interface APKManifestInfoRepository extends CrudRepository<APKManifestInfo, Long> {
	
	APKManifestInfo findByProjectId(long projectId);

}
