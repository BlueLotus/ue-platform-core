package org.iii.security.platform.core.persistence;

import org.iii.security.platform.core.domain.SystemConfig;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Repository
public interface SystemConfigRepository extends CrudRepository<SystemConfig, Long>{
}
