package org.iii.security.platform.core.util.constant;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class EmulatorJobConstant {
	
	public static final String APK_TOOL_COMMAND = "\\apktool.bat";
	public static final String EMULATOR_COMMAND = "\\tools\\emulator.exe";
	public static final String ADB_COMMAND = "\\platform-tools\\adb.exe";
	public static final String MONKEY_RUNNER_COMMAND = "\\tools\\monkeyrunner.bat";
	
	public static final String APK_TOOL_OPTION_DECOMPILE = "d";
	public static final String APK_TOOL_OPTION_OVERWRITE = "-f";
	public static final String APK_TOOL_OPTION_OUTPUT = "-o";
	
	public static final String EMULATOR_AVD_OPTION = "-avd";
	public static final String EMULATOR_SYSTEM_OPTION = "-system";
	public static final String EMULATOR_DATA_OPTION = "-data";
	public static final String EMULATOR_RAMDISK_OPTION = "-ramdisk";
	
	public static final String ADB_DEVICES_OPTION = "devices";
	
	public static final String ADB_LOGCAT_OPTION = "logcat";
	public static final String ADB_LOGCAT_VERBOSE = "-v";
	public static final String ADB_LOGCAT_TIME = "time";
	public static final String ADB_LOGCAT_TAINTLOG_WARNING = "TaintLog:W";
	public static final String ADB_LOGCAT_ACTIVITY_MGR_INFO = "ActivityManager:I";
	public static final String ADB_LOGCAT_ALERTSERVICE_DEBUG = "AlertService:D";
	public static final String ADB_LOGCAT_ALERTRECEIVER_DEBUG = "AlertReceiver:D";
	public static final String ADB_LOGCAT_ALL_SYSTEM_MSG = "*:S";
	
	public static final String ADB_INSTALL_OPTION = "install";
	public static final String ADB_UNINSTALL_OPTION = "uninstall";
	
	public static final String ADB_SHELL_OPTION = "shell";
	public static final String ADB_DUMPSYS = "dumpsys";
	public static final String ADB_DUMP_FOR_ACTIVITY = "activity";
	
	public static final String ADB_SHELL_SCREEN_CAPTURE = "screencap";
	public static final String ADB_SCREEN_CAPTURE_OPTION = "-p";
	public static final String ADB_SDCARD_PATH = "/sdcard/";
	
	public static final String ADB_PULL_OPTION = "pull";
	
	public static final String MONKEY_RUNNER_PLAYBACK_OPTION = "\\tools\\recorder_playback.py";
	public static final String MONKEY_RUNNER_SCRIPT_FOR_PLAY_BACK = "\\playback_script.txt";
	
	public static final String DEFAULT_TREE_DIAGRAM_RESULT = "{\"name\":\"unknown\",\"level\":\"darkgray\",\"value\":10,\"children\":[],\"detailurl\":\"\"}";
	public static final String STILL_ANALYZING = "{\"children\":[],\"name\":\"android.intent.action.VIEW cmp\"}";
	
	public static final int INSTALL_FAILED = 0;
	public static final int INSTALL_FAILED_BECAUSE_NO_AVAILABLE_PACKAGE_MANAGER = 1;
	public static final int INSTALL_FAILED_BECAUSE_APK_ALREADY_INSTALLED = 2;
	public static final int INSTALL_SUCCESSFULLY = 3;
	
	public static final int OBTAIN_EMULATOR_LAUNCH_PROCESS = 1;
	public static final int OBTAIN_LOGCAT_LAUNCHER = 2;
	public static final int OBTAIN_LOGCAT_LAUNCH_PROCESS = 3;
	public static final int OBTAIN_APK_FILE_INSTALLER_PROCESS = 4;
	public static final int OBTAIN_APK_MANIFEST_ANALYZER_PROCESS = 5;
	public static final int OBTAIN_SYSTEM_DUMP_PROCESS = 6;
	public static final int OBTAIN_ACTIVITY_SNAPSHOT_CATCHER_PROCESS = 7;
	public static final int OBTAIN_PULL_SNAPSHOT_PROCESS = 8;
	public static final int OBTAIN_MONKEY_RUNNER_SCRIPT_PLAYER_PROCESS = 9;
	public static final int OBTAIN_APK_FILE_UNINSTALLER_PROCESS = 10;
	
}
