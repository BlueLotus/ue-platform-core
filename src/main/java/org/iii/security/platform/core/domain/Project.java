package org.iii.security.platform.core.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Entity
@Table(name = "project_info")
public class Project implements Serializable {
	
	private static final long serialVersionUID = 1671324930415946409L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "project_name")
	private String projectName;
	
	@Column(name = "apk_name")
	private String apkName;
	
	@Column(name = "apk_upload_time")
	private Date apkUploadTime;
	
	@Column(name = "manifest_extracted")
	private boolean manifestExtracted;
	
	@Column(name = "established")
	private boolean established;
	
	@Column(name = "analysis_state")
	private String analysisState;
	
	@Column(name = "start_time", nullable = true)
	private Date startTime;
	
	@Column(name = "end_time", nullable = true)
	private Date endTime;
	
	@Column(name = "uptime", nullable = true)
	private long uptime;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getApkName() {
		return apkName;
	}
	public void setApkName(String apkName) {
		this.apkName = apkName;
	}

	public Date getApkUploadTime() {
		return apkUploadTime;
	}
	public void setApkUploadTime(Date apkUploadTime) {
		this.apkUploadTime = apkUploadTime;
	}

	public boolean isManifestExtracted() {
		return manifestExtracted;
	}
	public void setManifestExtracted(boolean manifestExtracted) {
		this.manifestExtracted = manifestExtracted;
	}

	public boolean isEstablished() {
		return established;
	}
	public void setEstablished(boolean established) {
		this.established = established;
	}

	public String getAnalysisState() {
		return analysisState;
	}
	public void setAnalysisState(String analysisState) {
		this.analysisState = analysisState;
	}

	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public long getUptime() {
		return uptime;
	}
	public void setUptime(long uptime) {
		this.uptime = uptime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((analysisState == null) ? 0 : analysisState.hashCode());
		result = prime * result + ((apkName == null) ? 0 : apkName.hashCode());
		result = prime * result + ((apkUploadTime == null) ? 0 : apkUploadTime.hashCode());
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result + (established ? 1231 : 1237);
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (manifestExtracted ? 1231 : 1237);
		result = prime * result + ((projectName == null) ? 0 : projectName.hashCode());
		result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
		result = prime * result + (int) (uptime ^ (uptime >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (analysisState == null) {
			if (other.analysisState != null)
				return false;
		} else if (!analysisState.equals(other.analysisState))
			return false;
		if (apkName == null) {
			if (other.apkName != null)
				return false;
		} else if (!apkName.equals(other.apkName))
			return false;
		if (apkUploadTime == null) {
			if (other.apkUploadTime != null)
				return false;
		} else if (!apkUploadTime.equals(other.apkUploadTime))
			return false;
		if (endTime == null) {
			if (other.endTime != null)
				return false;
		} else if (!endTime.equals(other.endTime))
			return false;
		if (established != other.established)
			return false;
		if (id != other.id)
			return false;
		if (manifestExtracted != other.manifestExtracted)
			return false;
		if (projectName == null) {
			if (other.projectName != null)
				return false;
		} else if (!projectName.equals(other.projectName))
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		if (uptime != other.uptime)
			return false;
		return true;
	}
	
	public Project finished() {
		this.analysisState = "finished";
		long now = System.currentTimeMillis();
		this.endTime = new Date(now);
		this.uptime = ((now - this.startTime.getTime()) / 60000);
		return this;
	}

	@Override
	public String toString() {
		return "Project [id=" + id + ", projectName=" + projectName
				+ ", apkName=" + apkName + ", apkUploadTime=" + apkUploadTime
				+ ", manifestExtracted=" + manifestExtracted + ", established="
				+ established + ", analysisState=" + analysisState
				+ ", startTime=" + startTime + ", endTime=" + endTime
				+ ", uptime=" + uptime + "]";
	}

}
