package org.iii.security.platform.core.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Entity
@Table(name = "apk_manifest_info")
public class APKManifestInfo implements Serializable {
	
	private static final long serialVersionUID = -8816233777251081868L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "project_id")
	private long projectId;
	
	@Column(name = "package_name")
	private String packageName;
	
	@Column(name = "uses_permission", nullable = true)
	private String usesPermission;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
	public String getUsesPermission() {
		return usesPermission;
	}
	public void setUsesPermission(String usesPermission) {
		this.usesPermission = usesPermission;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result
				+ ((packageName == null) ? 0 : packageName.hashCode());
		result = prime * result + (int) (projectId ^ (projectId >>> 32));
		result = prime * result
				+ ((usesPermission == null) ? 0 : usesPermission.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		APKManifestInfo other = (APKManifestInfo) obj;
		if (id != other.id)
			return false;
		if (packageName == null) {
			if (other.packageName != null)
				return false;
		} else if (!packageName.equals(other.packageName))
			return false;
		if (projectId != other.projectId)
			return false;
		if (usesPermission == null) {
			if (other.usesPermission != null)
				return false;
		} else if (!usesPermission.equals(other.usesPermission))
			return false;
		return true;
	}
	
	public List<String> transformUsesPermissionsToList(String usesPermissionStr) {
		return new ArrayList<String>(Arrays.asList(usesPermissionStr.split(",")));
	}
	
	@Override
	public String toString() {
		return "APKManifestInfo [id=" + id + ", projectId=" + projectId
				+ ", packageName=" + packageName + ", usesPermission="
				+ usesPermission + "]";
	}
	
}
