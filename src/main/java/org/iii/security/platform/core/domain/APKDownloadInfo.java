package org.iii.security.platform.core.domain;

import java.io.Serializable;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class APKDownloadInfo implements Serializable {
	
	private static final long serialVersionUID = -5022869988858982401L;
	
	private String packageName;
	private int versionCode;
	private int offerType;
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
	public int getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}
	
	public int getOfferType() {
		return offerType;
	}
	public void setOfferType(int offerType) {
		this.offerType = offerType;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + offerType;
		result = prime * result
				+ ((packageName == null) ? 0 : packageName.hashCode());
		result = prime * result + versionCode;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		APKDownloadInfo other = (APKDownloadInfo) obj;
		if (offerType != other.offerType)
			return false;
		if (packageName == null) {
			if (other.packageName != null)
				return false;
		} else if (!packageName.equals(other.packageName))
			return false;
		if (versionCode != other.versionCode)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return String.format("APK download info:\n"
				+ "  package name: %s\n"
				+ "  version code: %s\n"
				+ "  offer type: %s\n", packageName, versionCode, offerType);
	}
	
}
