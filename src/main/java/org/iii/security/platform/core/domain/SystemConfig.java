package org.iii.security.platform.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Entity
@Table(name = "system_config")
public class SystemConfig implements Serializable {
	
	private static final long serialVersionUID = 4532422103818483209L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "path_to_apk_tool")
	private String pathToApktool;
	
	@Column(name = "path_to_android_sdk")
	private String pathToAndroidSdk;
	
	@Column(name = "path_to_taint_droid_room")
	private String pathToTaintDroidRoom;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getPathToApktool() {
		return pathToApktool;
	}
	public void setPathToApktool(String pathToApktool) {
		this.pathToApktool = pathToApktool;
	}
	
	public String getPathToAndroidSdk() {
		return pathToAndroidSdk;
	}
	public void setPathToAndroidSdk(String pathToAndroidSdk) {
		this.pathToAndroidSdk = pathToAndroidSdk;
	}
	
	public String getPathToTaintDroidRoom() {
		return pathToTaintDroidRoom;
	}
	public void setPathToTaintDroidRoom(String pathToTaintDroidRoom) {
		this.pathToTaintDroidRoom = pathToTaintDroidRoom;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((pathToAndroidSdk == null) ? 0 : pathToAndroidSdk.hashCode());
		result = prime * result + ((pathToApktool == null) ? 0 : pathToApktool.hashCode());
		result = prime * result + ((pathToTaintDroidRoom == null) ? 0 : pathToTaintDroidRoom.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemConfig other = (SystemConfig) obj;
		if (id != other.id)
			return false;
		if (pathToAndroidSdk == null) {
			if (other.pathToAndroidSdk != null)
				return false;
		} else if (!pathToAndroidSdk.equals(other.pathToAndroidSdk))
			return false;
		if (pathToApktool == null) {
			if (other.pathToApktool != null)
				return false;
		} else if (!pathToApktool.equals(other.pathToApktool))
			return false;
		if (pathToTaintDroidRoom == null) {
			if (other.pathToTaintDroidRoom != null)
				return false;
		} else if (!pathToTaintDroidRoom.equals(other.pathToTaintDroidRoom))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "SystemConfig [id=" + id + ", pathToApktool=" + pathToApktool
				+ ", pathToAndroidSdk=" + pathToAndroidSdk
				+ ", pathToTaintDroidRoom=" + pathToTaintDroidRoom + "]";
	}
	
}
