package org.iii.security.platform.core.persistence;

import java.util.List;

import org.iii.security.platform.core.domain.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {
	
	Project findById(long id);
	
	List<Project> findByManifestExtractedIsFalse();
	
	List<Project> findByEstablishedIsTrueOrderByStartTimeDesc();
	
	List<Project> findByAnalysisStateOrderByStartTimeDesc(String analysisState);
	
	Page<Project> findByEstablishedIsTrueOrderByStartTimeDesc(Pageable pageable);
	
	Page<Project> findByEstablishedIsTrueAndAnalysisStateOrderByStartTimeDesc(String analysisState, Pageable pageable);
	
	Page<Project> findByEstablishedIsFalseAndAnalysisStateOrderByApkUploadTimeAsc(String analysisState, Pageable pageable);
	
}
