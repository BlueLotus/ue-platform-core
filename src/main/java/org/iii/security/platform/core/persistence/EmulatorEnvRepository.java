package org.iii.security.platform.core.persistence;

import java.util.List;

import org.iii.security.platform.core.domain.EmulationEnv;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Repository
public interface EmulatorEnvRepository extends CrudRepository<EmulationEnv, Long> {
	
	List<EmulationEnv> findAll();
	
	Page<EmulationEnv> findAll(Pageable pageable);
	
	List<EmulationEnv> findByScheduledIsTrue();

	List<EmulationEnv> findByScheduledIsFalse();
	
	List<EmulationEnv> findByAnalyticsProjectIdAndApkForAnalytics(long analyticsProjectId, String apkForAnalytics);

	List<EmulationEnv> findByScheduledIsTrueAndEmulatorLaunchedIsFalse();
	
	List<EmulationEnv> findByScheduledIsTrueAndEmulatorLaunchedIsTrue();
	
	List<EmulationEnv> findByScheduledIsTrueAndEmulatorLaunchedIsTrueAndLogcatLaunchedIsTrueAndApkInstalledIsFalse();
	
	List<EmulationEnv> findByScheduledIsTrueAndEmulatorLaunchedIsTrueAndLogcatLaunchedIsTrueAndCanInstallApkIsTrueAndApkInstalledIsTrue();

	List<EmulationEnv> findByScheduledIsTrueAndEmulatorLaunchedIsTrueAndLogcatLaunchedIsTrueAndCanInstallApkIsTrueAndApkInstalledIsFalse();
	
}
