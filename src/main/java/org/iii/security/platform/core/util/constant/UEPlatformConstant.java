package org.iii.security.platform.core.util.constant;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class UEPlatformConstant {
	
	public static final String UE_PLATFORM_HOME = "C:\\UE_analysis\\";
	public static final String APK_FILE_LOCATION = UE_PLATFORM_HOME + "samples\\";
	public static final String MONKEY_SCRIPT_FILE_LOCATION = UE_PLATFORM_HOME + "monkey_script\\";
	public static final String DECOMPILED_APK_FILE_LOCATION = UE_PLATFORM_HOME + "decompiled\\";
	public static final String LOGCAT_LOG_LOCATION = UE_PLATFORM_HOME + "analysis_log\\";
	public static final String DIR_FOR_APK_TO_BE_INSTALLED = UE_PLATFORM_HOME + "samples\\";
	public static final String ACTIVITY_SNAPSHOT_LOCATION = UE_PLATFORM_HOME + "snapshot\\";
	public static final String ACTIVITY_SNAPSHOT_NOT_FOUND = UE_PLATFORM_HOME + "snapshot\\error\\notFound.jpg";

}
