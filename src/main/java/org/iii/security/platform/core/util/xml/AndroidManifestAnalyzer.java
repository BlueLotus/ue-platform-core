package org.iii.security.platform.core.util.xml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParser;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class AndroidManifestAnalyzer extends XMLReader {
	
	private static final Logger logger = LoggerFactory.getLogger(AndroidManifestAnalyzer.class);
	
	private static final String ANDROID_MANIFEST_TAG_FOR_MANIFEST = "manifest";
	private static final String ANDROID_MANIFEST_ATTR_FOR_PACKAGE_NAME = "package";
	private static final String ANDROID_MANIFEST_TAG_FOR_USES_PERMISSION = "uses-permission";
	
	private boolean xmlLoaded = false;
	private String packageName;

	public AndroidManifestAnalyzer() {
		super();
	}
	
	public void loadManifest(String androidManifest) {
		try {
			xmlLoaded = setXML(androidManifest);
		} catch (IOException e) {
			logger.debug("Load manifest failed, the error message is: {}", e.getMessage());
		}
	}
	
	public List<String> extractAllPermissions() {
		List<String> permissions = new ArrayList<String>();
		if(xmlLoaded) {
			logger.debug("Extracting permissions from AndroidManifest.xml...");
			try {
				int type = getEventType();
				while(type != XmlPullParser.END_DOCUMENT) {
					if(type == XmlPullParser.START_TAG)
						if(getName().contains(ANDROID_MANIFEST_TAG_FOR_MANIFEST)) {
							for(int i = 0; i < getAttributeCount(); i++)
								if(getAttributeName(i).contains(ANDROID_MANIFEST_ATTR_FOR_PACKAGE_NAME))
									packageName = getAttributeValueWithAttrIndex(i);
						} else if(getName().contentEquals(ANDROID_MANIFEST_TAG_FOR_USES_PERMISSION)) {
							for(int i = 0; i < getAttributeCount(); i++) 
								permissions.add(getAttributeValueWithAttrIndex(i));
						}
					getNext();
					type = getEventType();
				}
				logger.debug("All permissions are extracted successfully.");
			} catch (IOException e) {
				logger.debug("Something wrong when extracting permissions, the error message is: {}", e.getMessage());
			}
		} else {
			logger.debug("Extract AndroidManifest.xml failed, please check your system environment.");
		}
		return permissions;
	}
	
	public String transformToUsesPermissionString(List<String> usesPermissions) {
		String usesPermissionString = "";
		for (String permission : usesPermissions) {
			usesPermissionString = usesPermissionString.concat(permission).concat(",");
		}
		return usesPermissionString.substring(0, usesPermissionString.length() - 1);
	}
	
	public String getPackageName() {
		return packageName;
	}

}
